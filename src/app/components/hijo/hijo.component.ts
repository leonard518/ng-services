import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.scss']
})
export class HijoComponent implements OnInit {

  @Input() nombreHijo: string = 'sin nombre';
  @Output() cambioNombreHijo = new EventEmitter<object>();

  constructor() { }

  ngOnInit(): void {
  }

  changeName() {
    this.nombreHijo = 'Alto Bardo';
    this.cambioNombreHijo.emit({name: 'leonard', age: 34})
  }

}
