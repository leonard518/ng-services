import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ServiceChangeService {
  private nameService: string = '';

  constructor() {}

  setName(name?: string): void {
    if (name === undefined) return;
    this.nameService = name;
  }

  getName(): string {
    return this.nameService;
  }
}
