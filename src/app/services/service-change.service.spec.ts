import { TestBed } from '@angular/core/testing';

import { ServiceChangeService } from './service-change.service';

describe('ServiceChangeService', () => {
  let service: ServiceChangeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceChangeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
