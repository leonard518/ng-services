import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './../components/components.module';
import { HomeComponent } from './home/home.component';
import { ViewNameComponent } from './view-name/view-name.component';




@NgModule({
  declarations: [
    HomeComponent,
    ViewNameComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule
  ]
})
export class PagesModule { }
