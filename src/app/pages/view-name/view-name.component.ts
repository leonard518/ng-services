import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceChangeService } from 'src/app/services/service-change.service';

@Component({
  selector: 'app-view-name',
  templateUrl: './view-name.component.html',
  styleUrls: ['./view-name.component.scss']
})
export class ViewNameComponent implements OnInit {

  name: string = '';

  constructor(
    private router: Router,
    private serviceChange: ServiceChangeService,
  ) { }

  ngOnInit(): void {

    this.name = this.serviceChange.getName();

  }

  go() {
    this.serviceChange.setName('Leomar Gonzalez')
    this.router.navigate(['home']);
  }

}
