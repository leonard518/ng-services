import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceChangeService } from 'src/app/services/service-change.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  eventObject: object = {};

  name: string = 'Leonard Gonzalez';
  nombre: string = 'Guillermo';

  constructor(
    private router: Router,
    private serviceChange: ServiceChangeService,
  ) { }

  ngOnInit(): void {
    this.getName()
  }

  next(){
    this.name = 'cambio';

    this.serviceChange.setName('Carla Torito');

    this.router.navigate(['view']);
  }

  changeName(){
    this.nombre = 'Cambio Loco!'
  }

  getName() {
    let name = this.serviceChange.getName()

    if (name !== '') {
      this.name = name;
    }
  }

}
